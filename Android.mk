LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := axel
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_SRC_FILES	:= axel.c conf.c conn.c ftp.c http.c search.c tcp.c text.c
LOCAL_CFLAGS := -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -O2
LOCAL_LDLIBS := -lpthread

include $(BUILD_EXECUTABLE)
